from django.shortcuts import render
from receipts.models import Receipt

# Create your views here.
def receipt_list(request):
    receipts = (
        Receipt.objects.all()
    )  # roa it is gonna pull all the receipt object, each would have all the attributes in it.
    context = {
        "receipt_list": receipts,  # this receipts after object.all is a list of receipts, which we give the name of receipt_list. both name are arbitrary
    }
    return render(request, "receipts/list.html", context)
